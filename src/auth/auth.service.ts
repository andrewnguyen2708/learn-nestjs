import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { AuthDto } from './dto';
import * as argon from 'argon2';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime/library';

@Injectable()
export class AuthService {
  constructor(private prisma: PrismaService) {}
  async signUp(dto: AuthDto) {
    try {
      // hash password
      const hash = await argon.hash(dto.password);

      // Create new user
      const user = await this.prisma.user.create({
        data: {
          email: dto.email,
          hash,
        },
      });

      delete user.hash;

      return user;
    } catch (error) {
      const isDulicateEmailError =
        error instanceof PrismaClientKnownRequestError &&
        error.code === 'P2002';
      if (isDulicateEmailError) {
        throw new ForbiddenException('Credentials taken');
        return;
      }
      throw error;
    }
  }

  async logIn(dto: AuthDto) {
    const user = await this.prisma.user.findUnique({
      where: {
        email: dto.email,
      },
    });

    if (!user) {
      throw new ForbiddenException('Credentials incorrect');
      return;
    }

    const isValidPassword = await argon.verify(user.hash, dto.password);
    if (!isValidPassword) {
      throw new ForbiddenException('Credentials incorrect');
      return;
    }

    delete user.hash;
    return user;
  }
}
